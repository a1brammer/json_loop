<html>
<script src="http://www.atmos.albany.edu/student/abrammer/jquery/jquery-2.1.1.min.js"></script>
<script src="http://www.atmos.albany.edu/student/abrammer/maps/jquery.detect_swipe.js"></script>
<link rel="stylesheet" type="text/css" href="http://www.atmos.albany.edu/student/abrammer/JsImageloop/JsImageLoop.css">
<script>

var scan="base"
var region="ENX"

function change_region(newregion){
  console.log(newregion);
  region=newregion; 
  delete useroptions.content[0].images
  refresh(useroptions.content)
}

function refresh(opts){
    $.each(opts, function(k, opt){
    console.log(k)
    text="";
    data= callimagenames(opt.scan,region);
    opt.title = opt.scan;
    opt.filenames = data[0];
    opt.values = data[0];
    opt.labels = data[1];
    delete(opt.images)
    loadimages(opt);
    });
    build_buttons(opts);
    draw(opts);
    check_loaded_buttons(opts);
    };

function callimagenames(scan, region){
   var filenames = new Array(); var labels = new Array();
   $.ajax({
       url:"image_list.php",
       type:"POST",
       data: "region="+ region+"&scan="+scan,
       success:function(msg){
           filenames = msg.files;
           labels = msg.names
       },
       dataType : "json",
       async:false
   });
   return [filenames, labels];
};

useroptions = {};           // This is needed. Curly Brackets !!!
useroptions.content = [];      // this initialises each looper.  Square brackets!!
useroptions.content.push({  //  options with brackets, comma between each variable. could all be on one line  
            prefix : "http://www.atmos.albany.edu/",
            scan : "base",
//             filenames : callimagenames(scan, region)[0],
            extension: "",
            title: scan,
//             labels : callimagenames(scan, region)[1],
            labels_on : true,
            label_interval: 12,
	        startingframe:12,
            }); 
useroptions.content.push({  //  options with brackets, comma between each variable. could all be on one line  
            prefix : "http://www.atmos.albany.edu/",
            scan : "stormrel",
            extension: "",
            title:  "stormrel",
            labels_on : true,
            label_interval: 12,
	        startingframe:12,
            }); 
useroptions.content.push({  //  options with brackets, comma between each variable. could all be on one line  
            prefix : "http://www.atmos.albany.edu/",
            scan : "compref",
            extension: "",
            title:  "compref",
            labels_on : true,
            label_interval: 12,
	        startingframe:12,
            }); 

$(function () {
$.each(useroptions.content, function(k,opt){
   data = callimagenames(opt.scan, region);
   opt.filenames = data[0];
   opt.labels = data[1];
    });    
});
    
$(document).ready(function(){
	var timer = setInterval( 'refresh(useroptions.content)', 60000);
//	play(useroptions['content']);
});       
 
</script>
<script src="http://www.atmos.albany.edu/student/abrammer/JsImageloop/JsImageLoop.dev.js"></script>


<body>

<div id='content' >
<form name='jumpto' method='POST' style='display:inline-block'>
<select style='width:95px' id='' onChange='change_region(this.value)'>
<option selected value="ENX">ENX</option>
<option value="BUF">BUF</option>
</select>
</form>
<button onmouseup='refresh(useroptions.content)'>Refresh</button>
</div>
</body>
</html>
