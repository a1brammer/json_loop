<html><head><title> North America | Alicia M. Bentley </title>
<script src="http://www.atmos.albany.edu/student/abrammer/jquery/jquery-2.1.1.min.js"></script>
<script src="http://www.atmos.albany.edu/student/abrammer/maps/jquery.detect_swipe.js"></script>
<script src="http://www.atmos.albany.edu/student/abrammer/JsImageloop/JsImageLoop.js"></script>
<link rel="stylesheet" type="text/css" href="http://www.atmos.albany.edu/student/abrammer/JsImageloop/JsImageLoop.css">
<script>

useroptions = {};
useroptions.content = [];

    useroptions['content'].push(
        {   title: "MSLP/200-hPa Jet/Thick.",
	        startingframe: 28,
            labels : fspan(-168,192,6) ,
            prefix : "http://www.atmos.albany.edu/student/abentley/realtime/images/northamer/mslp_jet/mslp_jet_",
            extension: ".gif",
            minval: 29,
            maxval: 89,
            increment:1,
            label_interval: 2,
        });
    useroptions['content'].push(
        {   title: "DT/925-850-hPa rel. vort.",
            startingframe: 28,
            labels : fspan(-168,192,6) ,
            prefix : "http://www.atmos.albany.edu/student/abentley/realtime/images/northamer/dt_2pvu/dt_2pvu_",
            extension: ".gif",
            minval: 29,
            maxval: 89,
            increment:1,
            label_interval: 2,
        });
    useroptions['content'].push(
        {   title: "PV/300-200-hPa irr. wind",
            startingframe: 28,
            labels : fspan(-168,192,6) ,
            prefix : "http://www.atmos.albany.edu/student/abentley/realtime/images/northamer/irro_wind/irro_wind_",
            extension: ".gif",
            minval: 29,
            maxval: 89,
            increment:1,
            label_interval: 2,
        });
    useroptions['content'].push(
        {   title: "500-hPa rel. vort., wind, etc.",
            startingframe: 28,
            labels : fspan(-168,192,6) ,
            prefix : "http://www.atmos.albany.edu/student/abentley/realtime/images/northamer/rel_vort/rel_vort_",
            extension: ".gif",
            minval: 29,
            maxval: 89,
            increment:1,
            label_interval: 2,
        });
    useroptions['content'].push(
        {   title: "700-hPa wind, etc./PW",
            startingframe: 28,
            labels : fspan(-168,192,6) ,
            prefix : "http://www.atmos.albany.edu/student/abentley/realtime/images/northamer/700wind_pw/700wind_pw_",
            extension: ".gif",
            minval: 29,
            maxval: 89,
            increment:1,
            label_interval: 2,
        });
            useroptions['content'].push(
        {   title: "850-hPa theta/900-800-hPa irr. wind",
            startingframe: 28,
            labels : fspan(-168,192,6) ,
            prefix : "http://www.atmos.albany.edu/student/abentley/realtime/images/northamer/irro_temp/irro_temp_",
            extension: ".gif",
            minval: 29,
            maxval: 89,
            increment:1,
            label_interval: 2,
        });
    useroptions['content'].push(
        {   title: "330 K PV, pressure, wind",
            startingframe: 28,
            labels : fspan(-168,192,6) ,
            prefix : "http://www.atmos.albany.edu/student/abentley/realtime/images/northamer/330K_isen/330K_isen_",
            extension: ".gif",
            minval: 29,
            maxval: 89,
            increment:1,
            label_interval: 2,
        });


        
</script>


</head><body>
</body>
</html>
