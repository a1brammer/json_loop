<html>
<script src="http://www.atmos.albany.edu/student/abrammer/jquery/jquery-2.1.1.min.js"></script>
<script src="http://www.atmos.albany.edu/student/abrammer/maps/jquery.detect_swipe.js"></script>
<script src="http://www.atmos.albany.edu/student/abrammer/JsImageloop/JsImageLoop.dev.js"></script>
<link rel="stylesheet" type="text/css" href="http://www.atmos.albany.edu/student/abrammer/JsImageloop/JsImageLoop.css">
<script>

<?
$root = "http://www.atmos.albany.edu/student/abrammer/graphics/gfs_realtime/2013/atl/vr_850/"  ;
$latest = file_get_contents($root."latest");
$latest = $latest - $latest%2;
$ti = strtotime("2013-01-01") ;
$latesti = intval($latest);
$newest = $latest;
$newesti = intval($newest);
echo "newesti=$newesti; ";

for($l=$latesti; $l>=605;$l--){
$datecodes[$l] = date("m/d Hi", $l*6*3600 + $ti );
};

?>


useroptions = {};
useroptions.content = [];
    useroptions['content'].push(
        {   title: "GFS",
            startingframe:4,
        //  startingvalue:12,
        //    values: [0,1],
            labels : fspan(0,240,24) ,
         //   filenames : [ 0, 1, 2, 3, 4,5 ,6, 7, 8, 9 ], // either links to each image
            timecode: newesti+".",
            prefix : "http://www.atmos.albany.edu/student/abrammer/graphics/gfs_realtime/2013/atl/vr_850/",
            extension: ".jpg",
            minval: 0,
            maxval: 40,
            increment:4,
	    colorbar: "colorbar.png"
        });

    useroptions['content'].push(
        {   title: "ECMWF",
         //   startingframe:4,
        //  startingvalue:12,
	  labels : fspan(0,240,24) ,
       //     filenames : [ 0, 8, 12, 16, 20, 24, 25,26,27,28,29  ], // either links to each image
           timecode: newesti+".",
 	   minval:0,
	   maxval:40,
	   increment:4,
	 //   prefix: "http://www.atmos.albany.edu/student/abrammer/maps/ecmwf/plots/rv850_sf_mslp/rv850_sf_mslp_",
            prefix : "http://www.atmos.albany.edu/student/abrammer/graphics/gfs_realtime/2013/atl/EC_vr/",
            extension: ".jpg",
            colorbar: "colorbar.png"
        });
        
        
       function archive(selector){
         divid=$(selector).parent().closest("div").attr('id');
         console.log( $(selector).parent().closest("div").attr('id') );
         $.each( useroptions[divid], function(i){
            this.timecode = selector.value+".";
            build_input(this);
         });
         load(useroptions[divid]);
         $( '#'+divid ).focus();
         }
        
        
</script>


<body>
<div id='content' class="holder"  tabindex="0" > <font style="font-size:10; text-align:center"> Archive: </font> 
                 <?
                 echo "<form name='jumpto' method='POST' style='display:inline-block'>";
                 echo "<select style='width:95px' id='' onChange='archive(this)'>";
                 for($i=$latesti; $i>=866;$i-=2){
                 if($i==$latesti){echo "<option SELECTED value=$i > $datecodes[$i] </option>";}
                 else{
                 echo "<option value=$i > $datecodes[$i] </option>";}}
                 echo "</select></form>";
                 ?>
</div>
</body>
</html>
