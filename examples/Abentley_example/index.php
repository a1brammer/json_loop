<html>
<script src="http://www.atmos.albany.edu/student/abrammer/jquery/jquery-2.1.1.min.js"></script>
<script src="http://www.atmos.albany.edu/student/abrammer/maps/jquery.detect_swipe.js"></script>
<script src="http://www.atmos.albany.edu/student/abrammer/JsImageloop/JsImageLoop.dev.js"></script>
<link rel="stylesheet" type="text/css" href="http://www.atmos.albany.edu/student/abrammer/JsImageloop/JsImageLoop.css">
<style>
.holder ul li {
/* 
    font-size: 14px;
 */
}
</style>

<script>

useroptions = {};
useroptions.content = [];
    useroptions['content'].push(
        {   title: "mslp_jet",
	        startingframe: 26,
            labels : fspan(-156,192,6) ,
            label_interval: 4,
            prefix : "http://www.atmos.albany.edu/student/abentley/mapdisco/20141024/images/atlantic/mslp_jet/mslp_jet_",
            extension: ".gif",
            minval: 1,
            maxval: 59,
            increment:1,
        });
    useroptions['content'].push(
        {   title: "dt_2pvu",
            startingframe: 26,
            labels : fspan(-156,192,6) ,
            prefix : "http://www.atmos.albany.edu/student/abentley/mapdisco/20141024/images/atlantic/dt_2pvu/dt_2pvu_",
            extension: ".gif",
            minval: 1,
            maxval: 59,
            increment:1,
        });
    useroptions['content'].push(
        {   title: "irro_wind",
            startingframe: 26,
            labels : fspan(-156,192,6) ,
            prefix : "http://www.atmos.albany.edu/student/abentley/mapdisco/20141024/images/atlantic/irro_wind/irro_wind_",
            extension: ".gif",
            minval: 1,
            maxval: 59,
            increment:1,
        });
    useroptions['content'].push(
        {   title: "rel_vort",
            startingframe: 26,
            labels : fspan(-156,192,6) ,
            prefix : "http://www.atmos.albany.edu/student/abentley/mapdisco/20141024/images/atlantic/rel_vort/rel_vort_",
            extension: ".gif",
            minval: 1,
            maxval: 59,
            increment:1,
        });
    useroptions['content'].push(
        {   title: "700wind_pw",
            startingframe: 26,
            labels : fspan(-156,192,6) ,
            prefix : "http://www.atmos.albany.edu/student/abentley/mapdisco/20141024/images/atlantic/700wind_pw/700wind_pw_",
            extension: ".gif",
            minval: 1,
            maxval: 59,
            increment:1,
        });
    useroptions['content'].push(
        {   title: "330K_isen",
            startingframe: 26,
            labels : fspan(-156,192,6) ,
            prefix : "http://www.atmos.albany.edu/student/abentley/mapdisco/20141024/images/atlantic/330K_isen/330K_isen_",
            extension: ".gif",
            minval: 1,
            maxval: 59,
            increment:1,
        });

        
</script>


<body>
</body>
</html>
